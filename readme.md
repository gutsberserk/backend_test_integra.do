<h1 align="center" id="top">Backend Test Integra.do</h1>
<br>

### <h2>🕹 Features</h2>

- [x] Registro de Universidade
- [x] Atualizar Universidade
- [x] Deletar Universidade
- [x] Listar Universidade
- [x] Listar Universidade por ID

### <h2>⚠ Pré-requisitos</h2>

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), [Node.js](https://nodejs.org/en/), [Mongo](https://www.mongodb.com/)
Além disto é bom ter um editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/).

### 🎲 Rodando o BackEnd (servidor)

```bash
# Clone este repositório
$ git clone <git@gitlab.com:gutsberserk/backend_test_integra.do.git>

# Acesse a pasta do projeto no terminal/cmd
$ cd backend_test_Integra.do

# Instale as dependências
$ yarn install

# Edite o arquivo .env para as suas variáveis de ambiente

# Popule seu banco
$ yarn populate

# Execute a aplicação
$ yarn dev

# O servidor inciará na porta:3000 ou a passada no .env - acesse <http://localhost:3000>
```

### <h2>🚦 Endpoints </h2>

<strong>Registrar universidades</strong>

- Endpoint: localhost:3000/universities - POST
- Body: O corpo da requisição deve conter as seguintes chaves = country,name,domains,alpha_two_code,state_province,web_pages
- Example: localhost:3000/universities
- Return: JSON da nova universidade criada.

<strong>Listar universidades</strong>

- Endpoint: localhost:3000/universities - GET
- Query params: page and country
- Example: localhost:3000/universities?country=Argentina&page=4
- Return: JSON com 20 universidades por paginação.

<strong>Listar universidade por ID</strong>

- Endpoint: localhost:3000/universities/:id - GET
- Example: localhost:3000/universities/622b9bc69ebc2451e6002dee
- Return: JSON com a universidade.

<strong>Atualizar universidade por ID</strong>

- Endpoint: localhost:3000/universities/:id - PUT
- Body : O corpo da requisição deve conter = web_pages,name,domains
- Example: localhost:3000/universities/622b9bc69ebc2451e6002dee
- Return: JSON com a universidade atualizada.

<strong>Deletar universidade por ID</strong>

- Endpoint: localhost:3000/universities/:id - DELETE
- Example: localhost:3000/universities/622b9bc69ebc2451e6002dee
- Return: Nenhuma - status 204.

### <h2> 🛠 Tecnologias </h2>

As seguintes ferramentas foram usadas na construção do projeto:

- [VSCode](https://code.visualstudio.com)
- [Mongo](https://www.mongodb.com/)
- [Node.js](https://nodejs.org/en/)
- [TypeORM-Mongo](https://orkhan.gitbook.io/typeorm/docs/mongodb)

### <h2> 🧑‍💻 Feito por </h2>

<div align="center">
<table align="center">
  <tr>
    <td align="center"><a href="https://gitlab.com/gutsberserk">
      <img src="https://media-exp1.licdn.com/dms/image/C5603AQHPdNWRu7A6LA/profile-displayphoto-shrink_200_200/0/1637589038106?e=1645056000&v=beta&t=8nV0Ljdnb1Mb5vPvBKDv7TslIh2FBsYYof7odKJdgss" style="border-radius: 50%" width="100px" alt="Imagem do perfil de Lucas Ribeiro"/>
      <br />
      <sub><b>Lucas Ribeiro</b></sub>
      <br />
    </td>
</table>
</div>

[Voltar para o topo 🔝](#top)
