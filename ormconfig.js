module.exports = {
  type: "mongodb",
  host: process.env.MONGO_DATABASE_HOST,
  port: process.env.MONGO_DATABASE_PORT,
  database: process.env.MONGO_DATABASE_NAME,
  synchronize: true,
  logging: false,
  entities: ["src/entities/**/*.ts"],
  migrations: ["src/migrations/**/*.ts"],
  subscribers: ["src/subscriber/**/*.ts"],
  cli: {
    entitiesDir: "src/entities",
    migrationsDir: "src/migrations",
    subscribersDir: "src/subscriber",
  },
};
