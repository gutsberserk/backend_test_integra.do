import { Entity, ObjectID, ObjectIdColumn, Column } from "typeorm";

@Entity()
export class University {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  country: string;

  @Column()
  name: string;

  @Column()
  domains: Array<string>;

  @Column()
  alpha_two_code: string;

  @Column()
  state_province: null | string;

  @Column()
  web_pages: Array<string>;
}
