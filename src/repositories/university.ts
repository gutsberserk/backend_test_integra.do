import { EntityRepository, Repository } from "typeorm";
import { University } from "../entities/university.entity";

@EntityRepository(University)
class UniversityRepository extends Repository<University> {}

export { UniversityRepository };
