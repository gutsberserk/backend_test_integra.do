import { UniversityRepository } from "../repositories/university";
import { getCustomRepository } from "typeorm";

interface Irequest {
  country: string | undefined;
}

class ListAllUniversitiesService {
  async execute({ country }: Irequest) {
    const universityRepository = getCustomRepository(UniversityRepository);

    if (country !== undefined) {
      const universities = await universityRepository.find({
        country: country,
      });
      if (universities.length === 0) {
        throw new Error("Country not found.");
      }
      return universities;
    }
    return universityRepository.find();
  }
}

export default ListAllUniversitiesService;
