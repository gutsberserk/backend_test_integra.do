import { UniversityRepository } from "../repositories/university";
import { getCustomRepository } from "typeorm";
import { ObjectId } from "mongodb";

interface IUniversityRequest {
  id: string;
  name: string;
  domains: Array<string>;
  web_pages: Array<string>;
}

class UpdateUniversityService {
  async execute({ id, name, domains, web_pages }: IUniversityRequest) {
    const universityRepository = getCustomRepository(UniversityRepository);

    const university = await universityRepository.find(new ObjectId(id));

    if (university === undefined) {
      throw new Error("Invalid ID.");
    }
    const update_university = university[0];

    update_university.name = name;
    update_university.domains = domains;
    update_university.web_pages = web_pages;

    await universityRepository.save(update_university);

    return update_university;
  }
}

export default UpdateUniversityService;
