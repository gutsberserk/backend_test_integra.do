import { UniversityRepository } from "../repositories/university";
import { getCustomRepository } from "typeorm";
import { ObjectId } from "mongodb";

class DeleteUniversityByIdService {
  async execute(id: string) {
    const universityRepository = getCustomRepository(UniversityRepository);

    const university = await universityRepository.find(new ObjectId(id));

    if (university[0] === undefined) {
      throw new Error("University not found.");
    }

    universityRepository.delete(university[0]);

    return {};
  }
}

export default DeleteUniversityByIdService;
