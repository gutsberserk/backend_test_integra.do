import { UniversityRepository } from "../repositories/university";
import { getCustomRepository } from "typeorm";

interface IUniversityRequest {
  country: string;
  name: string;
  domains: Array<string>;
  alpha_two_code: string;
  state_province: string | undefined;
  web_pages: Array<string>;
}

class RegisterUniversityService {
  async execute({
    country,
    name,
    domains,
    alpha_two_code,
    state_province,
    web_pages,
  }: IUniversityRequest) {
    const universityRepository = getCustomRepository(UniversityRepository);

    const alreadyExists = await universityRepository.findOne({
      name: name,
      country: country,
      state_province: state_province,
    });

    if (alreadyExists !== undefined) {
      throw new Error("University already registered.");
    }

    const new_university = universityRepository.create({
      country,
      name,
      domains,
      alpha_two_code,
      state_province,
      web_pages,
    });

    await universityRepository.save(new_university);

    return new_university;
  }
}

export default RegisterUniversityService;
