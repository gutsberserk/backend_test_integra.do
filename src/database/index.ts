import mongoose from "mongoose";
import dotenv from "dotenv";

dotenv.config();

const connectDatabase = () => {
  mongoose
    .connect(
      `mongodb://localhost/${process.env.MONGO_DATABASE_HOST}:${process.env.MONGO_DATABASE_PORT}/${process.env.MONGO_DATABASE_NAME}`
    )
    .then(() => {
      console.log("Connect in DB!");
    })
    .catch((err: any) => console.log(err));
};

export default connectDatabase;
