import { Response, NextFunction, Request } from "express";

export const updateUniversityKeysValidation = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const keys = req.body;

  const aceppt_keys = ["name", "domains", "web_pages"];

  const deny = (msg: string) => {
    return res.status(400).json({ message: msg });
  };

  if (Object.keys(keys).length < 3) {
    return res
      .status(400)
      .json({ message: `All keys must be passed.`, keys: aceppt_keys });
  }

  for (let key in keys) {
    if (aceppt_keys.includes(key) === false) {
      return deny(`The key ${key} is not accept.`);
    } else if (key === "web_pages" || key === "domains") {
      if (typeof keys[key] !== "object") {
        return deny(`The key ${key} must be an array.`);
      }
      if (Object.keys(keys[key]).length > 0) {
        for (let i in keys[key]) {
          if (typeof keys[key][i] !== "string") {
            return deny(`All the values in key ${key} must be a string.`);
          }
        }
      }
    } else if (typeof keys[key] !== "string") {
      return deny(`The key ${key} must be a string.`);
    }
  }
  next();
};
