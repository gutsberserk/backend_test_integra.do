import { Request, Response } from "express";
import DeleteUniversityByIdService from "../services/deleteUniversityById.service";

class DeleteUniversityByIdController {
  async handle(request: Request, response: Response) {
    const deleteUniversityByIdService = new DeleteUniversityByIdService();

    let { id }: any = request.params;

    try {
      const university = await deleteUniversityByIdService.execute(id);

      return response.status(204).json(university);
    } catch (error: any) {
      return response.status(400).json({ message: error.message });
    }
  }
}

export default DeleteUniversityByIdController;
