import { Request, Response } from "express";
import ListAllUniversitiesService from "../services/listAllUniversities.service";

class ListAllUniversitiesController {
  async handle(request: Request, response: Response) {
    const listAllUniversitiesService = new ListAllUniversitiesService();

    let { country, page }: any = request.query;

    if (!page) {
      page = 1;
    }

    try {
      const universities = await listAllUniversitiesService.execute({
        country,
      });

      const result: any = [];

      let index = (page - 1) * 20;
      let limit = index + 20;

      for (let i = index; i < limit; i++) {
        if (universities[i] === undefined) {
          break;
        }
        const { domains, alpha_two_code, web_pages, ...data } = universities[i];
        result.push(data);
      }
      if (result.length === 0) {
        throw new Error("Page not found.");
      }
      return response.json(result);
    } catch (error: any) {
      return response.status(400).json({ message: error.message });
    }
  }
}

export default ListAllUniversitiesController;
