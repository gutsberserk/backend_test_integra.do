import { Request, Response } from "express";
import ListUniversityByIdService from "../services/listUniversityById.service";

class ListUniversityByIdController {
  async handle(request: Request, response: Response) {
    const listUniversityByIdService = new ListUniversityByIdService();

    let { id }: any = request.params;

    try {
      const university = await listUniversityByIdService.execute(id);

      return response.json(university);
    } catch (error: any) {
      return response.status(400).json({ message: error.message });
    }
  }
}

export default ListUniversityByIdController;
