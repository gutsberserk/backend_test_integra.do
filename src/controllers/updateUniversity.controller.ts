import { Request, Response } from "express";
import UpdateUniversityService from "../services/updateUniversity.service";

class UpdateUniversityController {
  async handle(request: Request, response: Response) {
    try {
      const { name, domains, web_pages } = request.body;
      const { id } = request.params;

      const updateUniversityService = new UpdateUniversityService();

      const university = await updateUniversityService.execute({
        name,
        domains,
        web_pages,
        id,
      });

      return response.status(202).json(university);
    } catch (error: any) {
      return response.status(400).json({ message: error.message });
    }
  }
}

export default UpdateUniversityController;
