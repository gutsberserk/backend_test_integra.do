import { Request, Response } from "express";
import RegisterUniversityService from "../services/registerUniversity.service";

class RegisterUniversityController {
  async handle(request: Request, response: Response) {
    try {
      const {
        country,
        name,
        domains,
        alpha_two_code,
        state_province,
        web_pages,
      } = request.body;

      const registerUniversityService = new RegisterUniversityService();

      const university = await registerUniversityService.execute({
        country,
        name,
        domains,
        alpha_two_code,
        state_province,
        web_pages,
      });

      return response.status(201).json(university);
    } catch (error: any) {
      return response.status(400).json({ message: error.message });
    }
  }
}

export default RegisterUniversityController;
