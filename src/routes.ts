import { Router } from "express";
import ListAllUniversitiesController from "./controllers/listAllUniversities.controller";
import ListUniversityByIdController from "./controllers/listUniversityById.controller";
import RegisterUniversityController from "./controllers/registerUniversity.controller";
import UpdateUniversityController from "./controllers/updateUniversity.controller";
import DeleteUniversityByIdController from "./controllers/deleteUniversityById.controller";
import { universityKeysValidation } from "./middlewares/registerUniversityKeysValidation.middleware";
import { updateUniversityKeysValidation } from "./middlewares/updateUniversityKeysValidation.middleware";

const router = Router();

const listAllUniversitiesController = new ListAllUniversitiesController();
const listUniversityByIdController = new ListUniversityByIdController();
const registerUniversityController = new RegisterUniversityController();
const updateUniversityController = new UpdateUniversityController();
const deleteUniversityByIdController = new DeleteUniversityByIdController();

router.get("/universities", listAllUniversitiesController.handle);
router.get("/universities/:id", listUniversityByIdController.handle);
router.post(
  "/universities",
  universityKeysValidation,
  registerUniversityController.handle
);
router.put(
  "/universities/:id",
  updateUniversityKeysValidation,
  updateUniversityController.handle
);
router.delete("/universities/:id", deleteUniversityByIdController.handle);

export default router;
