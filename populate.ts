import axios from "axios";
import { createConnection, getConnectionOptions } from "typeorm";
import { University } from "./src/entities/university.entity";

const populate = async () => {
  const connectionOptions = await getConnectionOptions();
  const connection = await createConnection(connectionOptions);
  const repository = connection.getRepository<University>(University);

  const countrys = [
    "argentina",
    "brazil",
    "chile",
    "colombia",
    "paraguai",
    "peru",
    "suriname",
    "uruguay",
  ];

  const axios_api = axios.create({
    baseURL: "http://universities.hipolabs.com",
  });

  for (let country in countrys) {
    let universities: any = {};

    const req: any = await axios_api
      .get(`/search?country=${countrys[country]}`)
      .then((response) => (universities = response.data));

    let university: any;

    for (university in universities) {
      const newUniversity: any = new University();
      newUniversity.country = universities[university].country;
      newUniversity.name = universities[university].name;
      newUniversity.domains = universities[university].domains;
      newUniversity.alpha_two_code = universities[university].alpha_two_code;
      newUniversity.state_province = universities[university]["state-province"];
      newUniversity.web_pages = universities[university].web_pages;
      await repository.save(newUniversity);
    }
  }
  connection.close();
};

populate();
